<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoccerTeam extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * Lista todos os clientes de um time
     */
    public function clients()
    {
        return $this->hasMany(Client::class);
    }


}

//one-to-many = Um time pode ter muitos clientes