<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    const TYPE_INDIVIDUAL = 'individual';
    const TYPE_LEGAL      = 'legal';

    const MARITAL_STATUS = [
        1 => 'Solteiro',
        2 => 'Casado',
        3 => 'Divorciado'
    ];

    protected $fillable = [

        'name',
        'document_number',
        'email',
        'phone',
        'defaulter',
        'date_birth',
        'marital_status',
        'physical_disability',
        'company_name',
        'client_type',
        'soccer_team_id'

    ];

    /**
     * many_to_one - muitos para um
     * Exemplo: Relacionamento de clientes e times
     * Muitos clientes poderão ter um mesmo time (muitos para um)
     * ou
     * um time pode ter um ou mais clientes
     * many to one = muitos para um
     * belongsTo significa "pertence à"
     * Com esse método eu consigo pegar as informações do Team
     * que está vinculado ao cliente
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function soccerTeam()
    {
        return $this->belongsTo(SoccerTeam::class);
    }

    public function clientProfile()
    {
        return $this->hasOne(ClientProfile::class);
    }
}
